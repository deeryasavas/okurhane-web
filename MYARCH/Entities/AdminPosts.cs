﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MYARCH.Entities
{
    class AdminPosts : Base
    {
        public string admin_id { get; set; }
        public string createdOn { get; set; }
        public string isActive { get; set; }
        public string modifiedOn { get; set; }
        public string post_content { get; set; }
        public string post_image { get; set; }
        public string short_description{ get; set; }
        public string slug { get; set; }
        public string title { get; set; }
    }
}
