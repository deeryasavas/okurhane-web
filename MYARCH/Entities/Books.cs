﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MYARCH.Entities
{
    class Books : Base
    {
        public string book_author { get; set; }
        public string book_category { get; set; }
        public string book_image { get; set; }
        public string book_name { get; set; }
        public string book_rate { get; set; }
        public string date_of_issue { get; set; }
        public string page_number { get; set; }
        public string release_date { get; set; }
        public string short_description { get; set; }

    }
}
