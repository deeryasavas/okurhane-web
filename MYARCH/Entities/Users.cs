﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MYARCH.Entities
{
    class Users : Base
    {
        public string email { get; set; }
        public string full_name { get; set; }
        public string password { get; set; }
        public string user_comment { get; set; }
        public string user_image { get; set; }
        public string user_name { get; set; }

    }
}
